/* eslint-disable prettier/prettier */
import firebase from 'firebase';

class Fire {
  constructor() {
    this.init();
    this.checkAuth();
  }
  init = () => {
    if (!firebase.apps.length) {
      firebase.initializeApp({
       apiKey: 'AIzaSyAMDx9QlngOmmraV3yAbN5kYZ52vMxE6NI',
        authDomain: 'demo1-7e96e.firebaseapp.com',
        databaseURL:'https://demo1-7e96e-default-rtdb.firebaseio.com/',
        projectId: 'demo1-7e96e',
        storageBucket: 'demo1-7e96e.appspot.com',
        messagingSenderId: '1057032482873',
        appId: '1:1057032482873:web:e82804f858a0134d501de8',
        measurementId: 'G-JCENS0JD02',
      });
    }
  };

  checkAuth = () => {
    firebase.auth().onAuthStateChanged((user => {
      if (!user) {
        firebase.auth().signInAnonymously();
      }
    }));
  }

  send = messages => {
    messages.forEach(item=> {
      const message = {
        text: item.text,
        timestamp: firebase.database.ServerValue.TIMESTAMP,
        user: item.user,
      };

      this.db.push(message);
    });
  };

  parse = message => {
    const {user, text, timestamp} = message.val();
    const {key: _id} = message;
    const createdAt = new Date(timestamp);

    return {
      _id,
      createdAt,
      text,
      user,
    };
  };

  get = callback => {
    this.db.on('child_added', (snapshot) => callback(this.parse(snapshot)));
  };

  off() {
    this.db.off();
  }

  get db() {
    return firebase.database().ref('messages');
  }

  get uid() {
    return (firebase.auth().currentUser || {}).uid;
  }
}

export default new Fire();
